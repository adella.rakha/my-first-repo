from django.apps import AppConfig


class LabXConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lab_x'
